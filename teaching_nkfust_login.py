import urllib
import urllib2
import cookielib
import requests 
import sys
from bs4 import BeautifulSoup


def dataset():
    global payload
    global soup
    payload['__EVENTTARGETE'] = str(soup.find(id='__EVENTTARGET')['value'])
    payload['__EVENTARGUMENT'] = str(soup.find(id='__EVENTARGUMENT')['value'])
    payload['__VIEWSTATE'] = str(soup.find(id='__VIEWSTATE')['value'])
    payload['__VIEWSTATEGENERATOR'] = str(soup.find(id='__VIEWSTATEGENERATOR')['value'])
    payload['__PREVIOUSPAGE'] = str(soup.find(id='__PREVIOUSPAGE')['value'])
    payload['__EVENTVALIDATION'] = str(soup.find(id='__EVENTVALIDATION')['value'])


login_page = 'http://teaching.nkfust.edu.tw/Course/Login.aspx'
check_login='http://teaching.nkfust.edu.tw/Course/default.aspx'
data_page='http://teaching.nkfust.edu.tw/Course/student/today.aspx'
headers={"User-Agent":"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36"}
payload = {
        '__EVENTTARGET':'',
        '__EVENTARGUMENT':'',
        '__VIEWSTATE':'',
        '__VIEWSTATEGENERATOR':'',
        '__PREVIOUSPAGE':'',
        '__EVENTVALIDATION':'',
        'test':'',
        'Login1$LoginButton.x': '0',
        'Login1$LoginButton.y': '0',
        'Login1$UserName': '0324057',
        'Login1$Password': 'zxc------'
        }

s = requests.Session()
s.headers.update(headers)
first_get = s.get(login_page)
soup = BeautifulSoup(first_get.text, "html.parser")
dataset()
print payload
login_post = s.post(login_page,data=payload)
login_get = s.get(check_login)
today_get = s.get(data_page)
print today_get.status_code
print(today_get.text.encode(sys.stdin.encoding,"replace").decode(sys.stdin.encoding))
